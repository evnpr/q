QiscusRails::Application.routes.draw do
  post "discussion/likecomment"
  get "discussion/notify"
  match "discussion/getNotify/:code" => "discussion#getNotify"
  post "discussion/getNotify"
  match "discussion/notNotify/:code" => "discussion#notNotify"
  match "discussion/readnotif/:topic_id" => "discussion#readnotif"
  post "discussion/changeColor"

  post "discussion/sendInvitation"

  get "python/wordtokenize"
  post "python/wordtokenize"

  get "twitter/index"
  get "twitter/test"
  match "/quora" => "twitter#quora"
  post "twitter/quora"

  get "twitter/python"

  get "user/register"
  get "user/login"
  post "user/register"
  post "user/login"
  get "user/logout"
  get "user/edit"
  post "user/edit"
  get "user/changeColor"
  get "user/forget"
  post "user/forget"
  match "/resetPassword" => "user#resetPassword"

  get "site/index"
  get "site/home"
  match "site/room/:code" => "site#room"

  match "discussion/postcomment/:code" => "discussion#postcomment"
  post "discussion/gcmpostcomment"
  match "site/participant" => "site#participantlanding"
  match "site/room" => "site#roomlanding"

  match "site/pusher/:code" => "site#pusher"


  post "site/participantlanding"
  post "discussion/postcomment"
  get "site/pusher"

  post "site/newtopic"
  post "site/changeRoom"
  post "site/changeTopic"
  post "site/changeDesc"
  post "site/moveComment"

  match "site/roompoll/:code_encrypted/:firsttime" => "site#roompoll"
  post "site/roomcreate"

  match 'deleteComment/:id' => 'site#deleteComment', :via => :post

  post "discussion/compile"
  get "discussion/compile"

  post "discussion/counterNotifLive"

  post "discussion/commenthttp"

  match "activate/:id" => "user#activate"

  match "/verification_send" => "user#verification_send"


  match "/cloudmailin" => "discussion#cloudmailin", :via => :post

  get "discussion/translate"

  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  root :to => 'user#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end
