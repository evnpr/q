class Notifread < ActiveRecord::Base
  belongs_to :user
  belongs_to :notif
  attr_accessible :notif_id, :read, :user_id
end
