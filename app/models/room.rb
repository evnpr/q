class Room < ActiveRecord::Base
  belongs_to :user
  has_many :topics
  has_many :roompeoples
  attr_accessible :code, :name, :user_id
end
