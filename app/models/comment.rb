class Comment < ActiveRecord::Base
  belongs_to :user
  belongs_to :topic
  belongs_to :userdeletecomment
  has_many :likes
  has_many :highlights
  attr_accessible :message, :topic_id, :user_id
end
