class User < ActiveRecord::Base
  has_many :comments
  has_many :topics
  has_many :rooms
  has_many :notifreads
  has_many :likes
  has_many :highlights
  has_many :roompeoples
  has_many :userdeletecomments
  attr_accessible :email, :password, :username
end
