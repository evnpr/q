class Topic < ActiveRecord::Base
  belongs_to :room
  has_many :comments
  has_many :notifs
  attr_accessible :description, :room_id, :title
end
