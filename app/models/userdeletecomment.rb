class Userdeletecomment < ActiveRecord::Base
  belongs_to :user
  has_many :comments
  attr_accessible :comment_id, :user_id
end
