class Commenttranslate < ActiveRecord::Base
  belongs_to :comment
  attr_accessible :comment_id, :message, :user_id
end
