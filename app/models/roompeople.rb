class Roompeople < ActiveRecord::Base
  belongs_to :user
  belongs_to :room
  attr_accessible :room_id, :user_id
end
