class Notif < ActiveRecord::Base
  has_many :notifreads
  belongs_to :topic
  attr_accessible :message, :type
end
