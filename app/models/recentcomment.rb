class Recentcomment < ActiveRecord::Base
  belongs_to :room
  belongs_to :comment
  attr_accessible :comment_id, :room_id
end
