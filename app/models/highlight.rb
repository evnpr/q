class Highlight < ActiveRecord::Base
  belongs_to :user
  belongs_to :comment
  attr_accessible :color, :comment_id, :user_id
end
