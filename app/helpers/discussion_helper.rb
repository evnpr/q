module DiscussionHelper
    require 'uri'
    require 'net/http'
    require 'json'

    def mytranslate(comment, p, c, real_comment, username, tab, room_id, topic_id, code)
       input = comment
       source = "id"
       target = p.user.translate
       uri = URI.parse( "https://www.googleapis.com/language/translate/v2" )
       params = { :key => 'AIzaSyAdY1OOCbrbVYCTbtDzF5X8vYAkMZxLRsQ', :q => input, :target => target, :format => "text" }
       http = Net::HTTP.new(uri.host, uri.port) 
       if uri.scheme == "https"
         http.use_ssl = true
         http.verify_mode = OpenSSL::SSL::VERIFY_NONE
       end
       headers={'content-type'=>'applications/json'}
       http.get(uri.request_uri, headers)
       request = Net::HTTP::Get.new(uri.path) 
       request.set_form_data( params )

       # instantiate a new Request object
       request = Net::HTTP::Get.new( uri.path+ '?' + request.body ) 
       response = http.request(request)
       jdoc = JSON.parse(response.body)
       #commenttranslated = jdoc.fetch("data").first.fetch("translations").first.fetch("translatedText")
       commenttranslated = jdoc['data']['translations'].first['translatedText']
       ct = Commenttranslate.new(:user_id => p.user.id, :comment_id => c.id, :message => commenttranslated)
       ct.save
       Pusher[code].trigger_async('postcomment'+p.user.username, {
         :comment => commenttranslated,
         :real_comment => real_comment,
         :username => username,
         :username_real => c.user.username,
         :created_at => Time.now.strftime("%Y-%m-%d %H:%M"), 
         :comment_id => c.id,
         :room_id => room_id,
         :topic_id => topic_id,
         :topic_title => Topic.find(topic_id).title,
         :code => code
       })
    end
end
