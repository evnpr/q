require 'pusher'
require 'uri'


Pusher.app_id = '40098'
Pusher.key    = '896d049b53f1659213a2'
Pusher.secret = '6b172acc1115400f06ee'

class SiteController < ApplicationController

  caches_action :roompoll, :cache_path => Proc.new { |c| c.params }
  skip_before_filter :verify_authenticity_token

  def authenticate
    if cookies[:username] != 'guest' and cookies[:username] != nil
        cookies[:username] = 'guest'
    end
  end


  def index
    if cookies[:username] != 'guest' and cookies[:username] != nil
        redirect_to "/site/home"
    end
  end

  def home
    if cookies[:username] == 'guest' or cookies[:username] == nil
        redirect_to "/" and return
    end
    if Rails.cache.fetch(@username) == nil
        @username = cookies[:username]
        u = User.where(:username => @username).first
        if u.verified == 0
            @notverified = 1
        end
        if u.user_type == 1
            @user_type = 1
        end
        @username_visual = u.username_visual
        if !request.GET['mobile']
            @help = 1
        end
        rp = Roompeople.where(:user_id => u.id)
        nr = Notifread.where(:user_id => u.id)
        @rooms = []
        gotCache = 0
        notyetread = 0
        topic_from_nr = []
        nr.each do |nri|
           if !nri.notif.topic.nil?
               topic_from_nr << nri.notif.topic 
           end
        end
        rp.each do |r|
           room = Room.find(r.room_id)
           topic_from_room = room.topics
           if !room.topics.nil?
               #if Rails.cache.read(room.code_encrypted, :namespace => "roomcomment")
               #    last_comment = Rails.cache.read(room.code_encrypted, :namespace => "roomcomment")
               #    gotCache = 1
               #else
               #     if gotCache == 0
               #         last_comment = REDIS.lrange(room.code_encrypted, 0, 3)
               #
               #
               #
                        last_comment = Recentcomment.where(:room_id => room.id).first
                        if !last_comment.nil?
                            comment_id = last_comment.comment.id
                        end
                        common = topic_from_room & topic_from_nr
                        if common.length > 0
                            notyetread = 1
                        end
                        

               #         Rails.cache.write(room.code_encrypted, last_comment, :namespace => "roomcomment")
               #     end
               #end
           end
           if last_comment != nil
               if notyetread == 1
                   room.name = "[NEW] " + room.name
               end
               @rooms << [comment_id, room, notyetread, last_comment]
           else
               @rooms << [0, room, 0, 0]
           end
           @rooms.sort!.reverse!
           notyetread = 0
           #Rails.cache.fetch(@username) { @rooms }
        end
    else
        @rooms = Rails.cache.fetch(@username)
    end
  end

  def roomlanding
    if request.post?
        code = params[:code]
        username_cookies = params[:username_cookies]
        if Room.exists?(:code => code)
            room = Room.where(:code => code).first
            session[room.code_encrypted] = 1
            if params[:mobile] == 'android'
                r = Room.where(:code => code).first
                code_en = r.code_encrypted
                if Rails.cache.fetch(code_en+"poll", :namespace => "room") == nil
                      def roompoll1(code, firsttime, username_cookies)
                        if firsttime == 2
                            @showdelete = 1
                        end
                        if cookies[:username] == nil
                            cookies[:username] = username_cookies
                        end

                        u = User.where(:username => cookies[:username]).first
                        room = Room.where(:code_encrypted => code).first
                        topics = room.topics.order("created_at")
                        nr = u.notifreads
                        nrList = Hash.new(0)
                        nr.each do |nri|
                            nrList[nri.notif.topic_id] = nrList[nri.notif.topic_id] + 1
                        end
                        nrListoutput = []
                        nrList.each do |nri|
                            nrListoutput << [nri[0], nri[1]]
                        end
                        comments = []
                        topic = []
                        topics.each do |t|
                           t.comments.order("created_at ASC").each do |c|
                                if c.highlights.count > 0 
                                    color = "rgba(254, 212, 87,0.36)"
                                elsif c.deleted == true
                                    if @showdelete
                                        color = "#bebebe"
                                    else
                                        next
                                    end
                                else
                                    color = ""
                                end
                                if c.username_as == nil
                                    username_as = c.user.username_visual
                                else
                                    username_as = c.username_as
                                end

                                comment = c.message
                                if comment.include? "http"
                                  comment = comment.split()
                                  mycom = []
                                  comment.each do |com|
                                        if com.include? "http"
                                            if com.include? "image:" 
                                                com = com.split("image:")[1]
                                                com = "<img src='"+com+"' />"
                                                mycom << com
                                            else
                                                com = "<a href='"+com+"'>"+com+"</a><br>"
                                                mycom << com
                                            end
                                        else
                                            mycom << CGI::escapeHTML(com)
                                        end
                                  end
                                  comment = mycom.join(" ")
                                end
                                if c.userdeletecomment != nil
                                    deleter =  c.userdeletecomment.user.username
                                else
                                    deleter = 0
                                end
                                comments << [comment, username_as, c.id, c.created_at.strftime("%Y-%m-%d %H:%M"), c.likes.count, color, c.user.username, deleter]
                           end
                           if nrList[t.id] > 0
                                t.title = "[#{nrList[t.id].to_s}]" + " " + t.title
                           end
                           topic << [t.title, t.description, comments, t.id] 
                           comments = []
                        end
                        output = {:topics => topic, :notifread => nrListoutput, :code => code}
                        Rails.cache.write(code+"poll", output, :namespace => "room")
                      end
                      roompoll1(code_en,1,username_cookies)
                    
                end
                render :json => Rails.cache.fetch(code_en+"poll", :namespace => "room").to_json and return
            end
            redirect_to "/site/room/"+room.code_encrypted
        else
            flash[:error] = 'not exists'
            redirect_to request.referer
        end
    end
  end

  def roomcreate
    myloop = 1
    mobile = ''
    mobile = params[:mobile]
    name = params[:name]
    name = HTML::FullSanitizer.new.sanitize(name)
    if mobile == 'android'
        username = params[:username]
        username = URI.unescape(username)
    else
        username = cookies[:username]
    end
    description = params[:description]
    while myloop == 1 do 
        randomcode = (0...4).map{65.+(rand(26)).chr}.join.downcase
        if !Room.exists?(:code => randomcode)
            r = Room.new(:code => randomcode)
            codeencrypted = Digest::MD5.hexdigest(randomcode)
            r.code_encrypted = "3e"+codeencrypted+"qe2"
            r.name = name
            r.user_id = User.where(:username => username).first.id
            r.room_type = 0
            if params[:room_type]
                r.room_type = params[:room_type]
            end
            r.save
            t = Topic.new(:title => name)
            t.room_id = r.id
            t.description = description
            t.save
            user_id = User.where(:username => username).first.id
            rp = Roompeople.new(:room_id => r.id, :user_id => user_id)
            rp.save
            if mobile == "android"
                mobileoutput = {
                    :code => r.code, 
                    :code_encrypted => r.code_encrypted, 
                }
                render :json => mobileoutput and return
            end
            redirect_to "/site/room/"+r.code_encrypted and return
        end
    end
    redirect_to "/site/home"
  end

  def room 
    if cookies[:username] == nil
            cookies[:username] = 'guest'    
    end
    @username = cookies[:username]
    @user = User.where(:username => @username).first
    if @user.verified == 0
        @notverified = 1
    end
    if @user.translate == "0"
        @nontranslate = 1
    end
    code = params[:code]
    if code == 'd8Q1XO'
        @help = 1
    end
    @firstload = Rails.cache.fetch(code+"poll", :namespace => "room").to_json
    
    if !Room.exists?(:code_encrypted => code)
        redirect_to "/" and return
    end
    room = Room.where(:code_encrypted => code).first
    if room.room_type == 1
        @canDelete = 0
    end
    if room.room_type == 1 and room.user.username == cookies[:username]
        @canShowDelete = 1
    end
    if code == "fcd" or code == "d8Q1XO"
        session[code] = 1
    end
    if Roompeople.exists?(:user_id => @user.id, :room_id => room.id) or session[code] == 1 
        @allowpost = 1
    end
    @room = room
    @room_id = room.id
    if Roompeople.exists?(:user_id => @user.id, :room_id => @room_id)
            @follow = 1
    end
    if request.post?
        topic_id = params[:topic_id]
        comment = params[:comment]
        c = Comment.new(:topic_id => topic_id)
        c.message = comment
        c.save
    else
        @code = code
        @code_unencrypted = room.code
        if !Room.exists?(:code_encrypted => @code)
            redirect_to "/site/home" and return
        end
        if Topic.exists?(:room_id => room.id)
            @topics = Room.where(:code_encrypted => @code).first.topics
        end
    end
    @back = request.referer
    if request.GET['newtopic']
        render "newtopic" and return
    end
  end


  #for polling the result of discussion
  def roompoll
    code = params[:code_encrypted] 
    firsttime = params[:firsttime].to_i
    if firsttime == 2
        @showdelete = 1
    end

    #def checkNotif
    #    user = User.first
    #    numnotif = user.notifreads.where(:read => true).count
    #    if numnotif > 0
    #        return true
    #    else
    #        return false
    #    end
    #end

    #if firsttime == 0
    #    start = Time.now.to_i
    #    while checkNotif == false
    #        ended = Time.now.to_i
    #        delta = ended - start
    #        if delta > 0
    #            break
    #        end
    #        sleep(1)
    #    end
    #end

    u = User.where(:username => cookies[:username]).first
    room = Room.where(:code_encrypted => code).first
    topics = room.topics.order("created_at")
    nr = u.notifreads
    nrList = Hash.new(0)
    nr.each do |nri|
        nrList[nri.notif.topic_id] = nrList[nri.notif.topic_id] + 1
    end
    nrListoutput = []
    nrList.each do |nri|
        nrListoutput << [nri[0], nri[1]]
    end
    comments = []
    topic = []
    topics.each do |t|
       t.comments.order("created_at ASC").each do |c|
            if c.highlights.count > 0 
                color = "rgba(254, 212, 87,0.36)"
            elsif c.deleted == true
                if @showdelete
                    color = "#bebebe"
                else
                    next
                end
            else
                color = ""
            end
            if c.username_as == nil
                username_as = c.user.username_visual
            else
                username_as = c.username_as
            end

            if u.translate == "0"
                comment = c.message
            else
                if Commenttranslate.exists?(:user_id => u.id, :comment_id => c.id)
                    comment = Commenttranslate.where(:user_id => u.id, :comment_id => c.id).first.message
                else
                    comment = c.message
                end
            end
            if comment.include? "http"
              comment = comment.split()
              mycom = []
              comment.each do |com|
                    if com.include? "http"
                        if com.include? "image:" 
                            com = com.split("image:")[1]
                            com = "<img src='"+com+"' />"
                            mycom << com
                        else
                            com = "<a href='"+com+"'>"+com+"</a><br>"
                            mycom << com
                        end
                    else
                        mycom << CGI::escapeHTML(com)
                    end
              end
              comment = mycom.join(" ")
            end
            if c.userdeletecomment != nil
                deleter =  c.userdeletecomment.user.username
            else
                deleter = 0
            end
            comments << [comment, username_as, c.id, c.created_at.strftime("%Y-%m-%d %H:%M"), c.likes.count, color, c.user.username, deleter]
       end
       topic << [t.title, t.description, comments, t.id] 
       comments = []
    end
    output = {:topics => topic, :notifread => nrListoutput, :code => code}
    Rails.cache.write(code+"poll", output, :namespace => "room")
    render :json => output and return
  end


  def newtopic
    code = params[:code]
    title = params[:title]
    description = params[:description]
    mobile = params[:mobile]
    if mobile == 'android'
        room = Room.where(:code => code).first
        code = room.code_encrypted
        cookies[:username] = params[:username_cookies]
    else
        room = Room.where(:code_encrypted => code).first
    end
    expire_action(:controller => 'site', :action => 'roompoll', :code_encrypted => code, :firsttime => 0)
    expire_action(:controller => 'site', :action => 'roompoll', :code_encrypted => code, :firsttime => 1)
    expire_action(:controller => 'site', :action => 'roompoll', :code_encrypted => code, :firsttime => 2)
    if room.room_type != 0 and room.user.username != cookies[:username] 
        redirect_to request.referer and return
    end
    t = Topic.new(:title => title)
    t.description = "desc : "+description
    t.room_id = Room.where(:code_encrypted => code).first.id
    t.save
    Pusher[code].trigger('newtopic', {
      :updated => "updated"
    })
    ############GCM START###########################
    registration_id = 'APA91bEaktdlTIEa0XCOyYLMhICglmwu7qTQYXPcds7xMVu5Ma4l9d300_wnV5xtyUfEVTR9L51TJiEeUdo3mpzpieNXeEYWGHUZhxUp6vzdbwmTcdREmxFqnD-Pc9TchjHTR-y14G8Kkl9ivZqbC4NTT9-NUlSYVj28j71bPoxDJE3C5rry6wk'
    message = 'test'
    code = 'asdf'
    username = 'username'
    time = Time.now
    rp = Roompeople.where(:room_id => room.id)
    gcm_ids = []
    rp.each do |rps|
        gcm_id = rps.user.gcm_id
        if gcm_id != "0"
            gcm_ids << gcm_id
        end
    end
    url = "https://android.googleapis.com/gcm/send"
    uri = URI.parse(url)
    http = Net::HTTP.new(uri.host, uri.port)
    if uri.scheme == "https"
      http.use_ssl = true
      http.verify_mode = OpenSSL::SSL::VERIFY_NONE
    end
    messages = {
            :room_id => room.id,
            :topic_id => t.id,
    }
    fields = { 
        :registration_ids => gcm_ids,
        :data => {
            :message => messages,
        },
    }
    payload = fields.to_json
    myrequest = Net::HTTP::Post.new(uri.request_uri)
    myrequest["Content-Type"] = "application/json"
    myrequest["X-HTTP-Method-Override"] = "post"
    googleapikey = 'AIzaSyABO9gZ7_aoMYNZgzbxIhEvRaJ22fPXRXc'
    myrequest["Authorization"] = "key="+googleapikey
    myrequest.body = payload
    ############GCM END#############################
    if mobile == 'android'
        render :json => 'success' and return
    end
    if params[:back] == '' or params[:back] == nil
        redirect_to request.referer and return
    else
        redirect_to params[:back] and return
    end
  end

  def changeTopic
    topic_id = params[:topicID]
    topicTitle = params[:topicTitle]
    code = params[:code]
    room = Room.where(:code_encrypted => code).first
    if room.room_type != 0 and room.user.username != cookies[:username] 
        render :nothing => true and return
    end
    expire_action(:controller => 'site', :action => 'roompoll', :code_encrypted => code, :firsttime => 0)
    expire_action(:controller => 'site', :action => 'roompoll', :code_encrypted => code, :firsttime => 1)
    expire_action(:controller => 'site', :action => 'roompoll', :code_encrypted => code, :firsttime => 2)
    seconds = params[:seconds]
    t = Topic.find(topic_id)
    t.title = topicTitle
    if topicTitle == '' or topicTitle == '<br>'
        t.comments.each do |tc|
            if Recentcomment.exists?(:comment_id => tc.id)
                rc = Recentcomment.where(:comment_id => tc.id).first
                rc.delete
            end
            tc.delete
        end
        t.delete
    else
        t.save
    end
    Pusher[code].trigger('changetopic', {
        :seconds => seconds,
        :topic_id => topic_id
    })
    redirect_to "/"
  end

  def changeDesc
    topic_id = params[:topicID]
    topicDesc = params[:topicDesc]
    code = params[:code]
    room = Room.where(:code_encrypted => code).first
    if room.room_type != 0 and room.user.username != cookies[:username] 
        render :nothing => true and return
    end
    expire_action(:controller => 'site', :action => 'roompoll', :code_encrypted => code, :firsttime => 0)
    expire_action(:controller => 'site', :action => 'roompoll', :code_encrypted => code, :firsttime => 1)
    expire_action(:controller => 'site', :action => 'roompoll', :code_encrypted => code, :firsttime => 2)
    seconds = params[:seconds]
    t = Topic.find(topic_id)
    if topicDesc.length < 10
        topicDesc = " - "
    end
    t.description = topicDesc
    t.save
    Pusher[code].trigger('changedesc', {
        :seconds => seconds,
        :topic_id => topic_id
    })
    redirect_to "/"
  end

  def changeRoom
        roomName = params[:roomName]
        roomName = HTML::FullSanitizer.new.sanitize(roomName)
        if roomName == '' or roomName.length < 5
            return
        end
        room_id = params[:roomID]
        r = Room.find(room_id)
        if r.room_type != 0 and r.user.username != cookies[:username] 
            render :nothing => true and return
        end
        r.name = roomName
        r.save
        render :nothing => true and return
  end

  def participantlanding
    if request.post?
        code = params[:code]
        if Room.exists?(:code => code)
            redirect_to "/site/participant/"+code
        else
            flash[:error] = 'not exists'
            redirect_to request.referer
        end
    end
  end



  def pusher
    code = params[:code]
    Pusher[code].trigger('delete', {
    })
    render :nothing => true
  end



  def deleteComment
    code = params[:code]
    mobile = params[:mobile]
    if mobile == 'android'
        room = Room.where(:code => code).first
        code = room.code_encrypted
        cookies[:username] = params[:username_cookies]
    else
        room = Room.where(:code_encrypted => code).first
    end
    expire_action(:controller => 'site', :action => 'roompoll', :code_encrypted => code, :firsttime => 0)
    expire_action(:controller => 'site', :action => 'roompoll', :code_encrypted => code, :firsttime => 1)
    expire_action(:controller => 'site', :action => 'roompoll', :code_encrypted => code, :firsttime => 2)
    u = User.where(:username => cookies[:username]).first
    c = Comment.find(params[:id])
    if room.room_type != 0 and room.user.username != cookies[:username] and c.user.username != cookies[:username] 
        render :nothing => true and return
    end
    if room.room_type != 0 and c.user.username == 'guest' and room.user.username != cookies[:username]
        render :nothing => true and return
    end
    if Recentcomment.exists?(:comment_id => c.id)
        rc = Recentcomment.where(:comment_id => c.id).first
        rc.delete
    end
    c.deleted = true
    usd = Userdeletecomment.new(:user_id => u.id, :comment_id => c.id)
    usd.save
    c.userdeletecomment_id = usd.id
    c.save
    Pusher[code].trigger('delete', {
        :comment_id => params[:id],    
    })


    rp = Roompeople.where(:room_id => room.id)
    gcm_ids = []
    rp.each do |rps|
        gcm_id = rps.user.gcm_id
        if gcm_id != "0"
            gcm_ids << gcm_id
        end
    end
    url = "https://android.googleapis.com/gcm/send"
    uri = URI.parse(url)
    http = Net::HTTP.new(uri.host, uri.port)
    if uri.scheme == "https"
      http.use_ssl = true
      http.verify_mode = OpenSSL::SSL::VERIFY_NONE
    end
    messages= {
            :comment_id => c.id,
            :code => room.code,
    }
    fields = { 
        :registration_ids => gcm_ids,
        :data => {
            :message => messages,
        },
    }
    payload = fields.to_json
    myrequest = Net::HTTP::Post.new(uri.request_uri)
    myrequest["Content-Type"] = "application/json"
    myrequest["X-HTTP-Method-Override"] = "post"
    googleapikey = 'AIzaSyABO9gZ7_aoMYNZgzbxIhEvRaJ22fPXRXc'
    myrequest["Authorization"] = "key="+googleapikey
    myrequest.body = payload
    response = http.request(myrequest)


    Rails.cache.delete(room.code_encrypted+"poll", :namespace => "room")
    render :json => 'success delete' and return
  end


  def moveComment
    topic_id = params[:topic_id]
    topic_id = topic_id.split("topic")[1].to_i
    mobile = params[:mobile]
    comment_id = params[:comment_id]
    comment_id = comment_id.split("theComment")[1].to_i
    code = params[:code]
    if mobile == 'android'
        topic_id = params[:topic_id]
        comment_id = params[:comment_id]
        room = Room.where(:code => code).first
        code = room.code_encrypted
    end
    c = Comment.find(comment_id)
    topic_id_before = c.topic_id
    c.topic_id = topic_id
    c.created_at = Time.now
    c.save
    Pusher[code].trigger('refresh', {
        :topic_id => topic_id,
        :comment_id => comment_id
    })
    if mobile == 'android'
        rp = Roompeople.where(:room_id => room.id)
        gcm_ids = []
        rp.each do |rps|
            gcm_id = rps.user.gcm_id
            if gcm_id != "0"
                gcm_ids << gcm_id
            end
        end
        url = "https://android.googleapis.com/gcm/send"
        uri = URI.parse(url)
        http = Net::HTTP.new(uri.host, uri.port)
        if uri.scheme == "https"
          http.use_ssl = true
          http.verify_mode = OpenSSL::SSL::VERIFY_NONE
        end
        messages= {
                :topic_id_before => topic_id_before,
                :comment_id => c.id,
                :code => room.code,
                :topic_id => topic_id,
        }
        fields = { 
            :registration_ids => gcm_ids,
            :data => {
                :message => messages,
            },
        }
        payload = fields.to_json
        myrequest = Net::HTTP::Post.new(uri.request_uri)
        myrequest["Content-Type"] = "application/json"
        myrequest["X-HTTP-Method-Override"] = "post"
        googleapikey = 'AIzaSyABO9gZ7_aoMYNZgzbxIhEvRaJ22fPXRXc'
        myrequest["Authorization"] = "key="+googleapikey
        myrequest.body = payload
        response = http.request(myrequest)
        render :json => 'success move comment' and return
    end
    render :text => topic_id and return
  end

end
