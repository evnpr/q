require 'uri'
require 'net/http'
require 'json'

class DiscussionController < ApplicationController
    
  Urbanairship.application_key = 'j199tHvNRzaNQT7Vd7Wd4w'
  Urbanairship.application_secret = 'oj8mJSryRA-68yutQj9QvA'
  Urbanairship.master_secret = 'HL59-zTWQ7mSLbcmPtyclQ'
  Urbanairship.logger = Rails.logger
  Urbanairship.request_timeout = 5 # defau

  uri = URI.parse(ENV["REDISTOGO_URL"])
  REDIS2 = Redis.new(:host => uri.host, :port => uri.port, :password => uri.password)
  
  skip_before_filter :verify_authenticity_token


  include DiscussionHelper


  def postcomment 
    code = params[:code]
    expire_action(:controller => 'site', :action => 'roompoll', :code_encrypted => code, :firsttime => 0)
    expire_action(:controller => 'site', :action => 'roompoll', :code_encrypted => code, :firsttime => 1)
    expire_action(:controller => 'site', :action => 'roompoll', :code_encrypted => code, :firsttime => 2)
    if request.post?
        username = params[:username].html_safe
        username = HTML::FullSanitizer.new.sanitize(username)
        @username = cookies[:username]
        if @username == nil or @username == ''
            @username = params[:username_cookies]
        end
        comment = CGI::escapeHTML(params[:comment])
        real_comment = params[:comment]
        tab = params[:tab]
        topic_id = params[:topic_id]
        room_id = params[:room_id]
        if username == ''
            return
        end
        r = Room.find(room_id)
        if comment == '' or comment == nil
            return
        end
        c = Comment.new(:topic_id => topic_id)
        c.message = comment
        colors = ['94FFA8','FC7EB1','94FAFF']
        u = User.where(:username => @username).first
        user_id = u.id
        c.user_id = user_id
        c.username_as = HTML::FullSanitizer.new.sanitize(username)
        c.save
        #REDIS.del(r.code_encrypted)
        #REDIS2.delay.rpush(r.code_encrypted, [c.message, c.user.username, c.topic.title, c.topic.id])
        if Recentcomment.exists?(:room_id => r.id)
            rc = Recentcomment.where(:room_id => r.id).first
            rc.comment_id = c.id
            rc.save
        else
            rc = Recentcomment.new(:room_id => r.id, :comment_id => c.id)
            rc.save
        end
        #Rails.cache.write(r.code_encrypted, [c.message, c.user.username, c.topic.title], :namespace => "roomcomment")
        tmpcomment = comment
        if comment.include? "http"
          comment = comment.split()
          mycom = []
          comment.each do |com|
                if com.include? "http"
                    if com.include? "image:" 
                        com = com.split("image:")[1]
                        com = "<img src='"+com+"' />"
                        mycom << com
                    else
                        com = "<a href='"+com+"'>"+com+"</a><br>"
                        mycom << com
                    end
                else
                    mycom << CGI::escapeHTML(com)
                end
          end
          comment = mycom.join(" ")
        end
        Pusher[code].trigger_async('postcomment', {
          :comment => comment,
          :real_comment => real_comment,
          :username => username,
          :username_real => c.user.username,
          :tab => tab,
          :created_at => Time.now.strftime("%Y-%m-%d %H:%M"), 
          :comment_id => c.id,
          :room_id => room_id,
          :topic_id => topic_id,
          :topic_title => Topic.find(topic_id).title,
          :code => code
        })
        rp = Roompeople.where(:room_id => room_id)
        ios_ids = []
        rp.each do |rps|
            ios_id = rps.user.ios_id
            if ios_id != "0" and ios_id != "(null)"
                ios_ids << ios_id
            end
        end
        notification = {
          :device_tokens => ios_ids,
          :aps => {:alert => "new message: #{comment}", :badge => 1},
          :topic => "#{r.name} - #{Topic.find(topic_id).title}",
          :code => r.code,
        }    
        if ios_ids != []
            Urbanairship.push(notification)
        end
        Rails.cache.delete(code+"poll", :namespace => "room")

        url = "https://android.googleapis.com/gcm/send"
        uri = URI.parse(url)
        http = Net::HTTP.new(uri.host, uri.port)
        if uri.scheme == "https"
          http.use_ssl = true
          http.verify_mode = OpenSSL::SSL::VERIFY_NONE
        end

        gcm_ids = []
        rp.each do |rps|
            gcm_id = rps.user.gcm_id
            if gcm_id != "0"
                gcm_ids << gcm_id
            end
        end
        messages = {
                :code => r.code,
                :username => username,
                :time => Time.now.strftime("%Y-%m-%d %H:%M"),
                :comment => comment,
                :real_comment => real_comment,
                :username => username,
                :username_real => c.user.username,
                :tab => tab,
                :created_at => Time.now.strftime("%Y-%m-%d %H:%M"), 
                :comment_id => c.id,
                :room_id => room_id,
                :topic_id => topic_id,
                :topic_title => Topic.find(topic_id).title,
                :code_en => code
        }

        fields = { 
            :registration_ids => gcm_ids,
            :data => {
                :message => messages,
            },
        }
        payload = fields.to_json
        myrequest = Net::HTTP::Post.new(uri.request_uri)
        myrequest["Content-Type"] = "application/json"
        myrequest["X-HTTP-Method-Override"] = "post"
        googleapikey = 'AIzaSyABO9gZ7_aoMYNZgzbxIhEvRaJ22fPXRXc'
        myrequest["Authorization"] = "key="+googleapikey
        myrequest.body = payload


        comment = tmpcomment
        if !Roompeople.exists?(:user_id => user_id, :room_id => room_id) and cookies[:username] != 'guest'
           rp = Roompeople.new(:user_id => user_id, :room_id => room_id)
           session[code] = 1
           rp.save
        end
        #HardWorker.perform_async(room_id, r, comment, topic_id, cookies[:username], code)
        rp = Roompeople.where(:room_id => room_id)
        rp.each do |p|
            if p.user.translate != "0"
                mytranslate(comment, p, c, real_comment, username, tab, room_id, topic_id, code).delay
            end
            if p.user.username == cookies[:username] || p.user.verified == 0
                next
            end
            n = Notif.new(:message => "got notifications from #{code} room")
            n.topic_id = topic_id
            n.save
            nr = Notifread.new(:user_id => p.user.id, :notif_id => n.id)
            nr.save
            mymail = {
                :recipient => p.user.username,
                :sender => cookies[:username],
                :message => comment,
                :room_name => r.name,
                :room_code => r.code,
                :room_code_encrypted => r.code_encrypted,
                :topic_id => topic_id,
                :host => "#{request.protocol}#{request.host_with_port}"
            }
            begin
                if !p.user.twitter.nil? and p.user.twitter != ''
                   if comment.length > 20
                        commentTwit = comment[0..20]+".."
                   else
                        commentTwit = comment
                   end
                   begin
                       messageTwit = commentTwit+" - "+cookies[:username]+" #{request.protocol}#{request.host_with_port}/site/room/#{r.code_encrypted}?t=#{topic_id}"
                       Twitter.delay.direct_message_create("#{p.user.twitter}", "#{messageTwit}")
                   end
                else
                    #UserMailer.delay.notification_email(mymail)
                end
            rescue Exception=>e
                test = 'does not do anything'
            end
            #UserMailer.delay.notification_email(mymail)
        end
        
    else
        if Room.where(:code => code).first.topics.exists?
           @topics = Room.where(:code => code).first.topics 
        end
    end
    @code = code
    render :nothing => true and return
  end

  def likecomment
        if request.post?
            code = params[:code]
            username = params[:username]
            comment_id = params[:comment_id]
            mobile = params[:mobile]
            if mobile == 'android'
                room = Room.where(:code => code).first
                code = Room.where(:code => code).first.code_encrypted
            end
            expire_action(:controller => 'site', :action => 'roompoll', :code_encrypted => code, :firsttime => 0)
            expire_action(:controller => 'site', :action => 'roompoll', :code_encrypted => code, :firsttime => 1)
            expire_action(:controller => 'site', :action => 'roompoll', :code_encrypted => code, :firsttime => 2)
            user_id = User.where(:username => username).first.id
            l = Like.new(:user_id => user_id, :comment_id => comment_id)
            l.save
        end
        ############GCM START###########################
        registration_id = 'APA91bEaktdlTIEa0XCOyYLMhICglmwu7qTQYXPcds7xMVu5Ma4l9d300_wnV5xtyUfEVTR9L51TJiEeUdo3mpzpieNXeEYWGHUZhxUp6vzdbwmTcdREmxFqnD-Pc9TchjHTR-y14G8Kkl9ivZqbC4NTT9-NUlSYVj28j71bPoxDJE3C5rry6wk'
        message = 'test'
        code = 'asdf'
        username = 'username'
        time = Time.now
        rp = Roompeople.where(:room_id => room.id)
        gcm_ids = []
        rp.each do |rps|
            gcm_id = rps.user.gcm_id
            if gcm_id != "0"
                gcm_ids << gcm_id
            end
        end
        url = "https://android.googleapis.com/gcm/send"
        uri = URI.parse(url)
        http = Net::HTTP.new(uri.host, uri.port)
        if uri.scheme == "https"
          http.use_ssl = true
          http.verify_mode = OpenSSL::SSL::VERIFY_NONE
        end
        fields = { 
            :registration_ids => gcm_ids,
            :data => {
                :username => username,
                :comment_id => comment_id,
            },
        }
        payload = fields.to_json
        myrequest = Net::HTTP::Post.new(uri.request_uri)
        myrequest["Content-Type"] = "application/json"
        myrequest["X-HTTP-Method-Override"] = "post"
        googleapikey = 'AIzaSyABO9gZ7_aoMYNZgzbxIhEvRaJ22fPXRXc'
        myrequest["Authorization"] = "key="+googleapikey
        myrequest.body = payload
        ############GCM END#############################
        if mobile == 'android'
            render :json => "#{username} success like comment id #{comment_id}" and return
        end
        render :nothing => true
  end

  def changeColor
        if request.post?
            comment_id = params[:comment_id]
            seconds = params[:seconds]
            code = params[:code]
    expire_action(:controller => 'site', :action => 'roompoll', :code_encrypted => code, :firsttime => 0)
    expire_action(:controller => 'site', :action => 'roompoll', :code_encrypted => code, :firsttime => 1)
    expire_action(:controller => 'site', :action => 'roompoll', :code_encrypted => code, :firsttime => 2)
            Pusher[code].trigger_async('changeColor', {
               :comment_id => comment_id, 
               :seconds => seconds,
            })
            username = params[:username]
            user_id = User.where(:username => username).first.id
            if Highlight.exists?(:user_id => user_id, :comment_id => comment_id)
                h = Highlight.where(:comment_id => comment_id)
                h.destroy_all
            else
                h = Highlight.new(:user_id => user_id, :comment_id => comment_id, :color => "FC7EB1")
                h.save
            end
        end
        render :nothing => true
  end

  def notify
    recipient = request.GET[:r]
    UserMailer.welcome_email(recipient).deliver
    render :text => recipient
  end


  def sendInvitation
    recipient = params[:email]
    sender = cookies[:username]
    message = params[:message]
    mobile = params[:mobile]
    if mobile == 'android'
        sender = params[:sender]
        recipient = params[:recipient]
    end
    mymail = {
        :recipient => recipient,
        :sender => sender,
        :message => message,
    }
    UserMailer.delay.invite_email(mymail)
    if mobile == 'android'
        render :json => "success send email from #{sender} to #{recipient}, the message is #{message}" and return
    end
    flash[:success] = "email delivered"
    redirect_to request.referer and return
  end



  def getNotify
    code = params[:code]
    mobile = params[:mobile]
    if mobile == 'android'
        cookies[:username] = params[:username_cookies]
        code_un = params[:code_un]
        code = Room.where(:code => code_un).first.code_encrypted
    end
    username = cookies[:username]
    if username == 'guest'
        flash[:error] = 'you have to login to be able to follow the room, please login <a href="/">here</a>'
        redirect_to request.referer and return
    end
    if mobile != "android"
        if !session[code]
            flash[:error] = 'you have to had the code to be able to follow this room'
            redirect_to request.referer and return
        end
    end
    user_id = User.where(:username => username).first.id
    room_id = Room.where(:code_encrypted => code).first.id
    if !Roompeople.exists?(:user_id => user_id, :room_id => room_id)
        rp = Roompeople.new(:user_id => user_id, :room_id => room_id)
        rp.save
        flash[:success] = 'success'
        if mobile == 'android'
            render :json => "success" and return
        end
        redirect_to request.referer and return
    end
    flash[:error] = 'you already follow'
    if mobile == 'android'
        render :json => "you already follow" and return
    end
    redirect_to request.referer and return
  end


  def notNotify
    code = params[:code]
    username = cookies[:username]
    if username == 'guest'
        flash[:error] = 'you have to login to be able to follow the room, please login <a href="/">here</a>'
        redirect_to request.referer and return
    end
    u = User.where(:username => username).first
    user_id = u.id
    room_id = Room.where(:code_encrypted => code).first.id
    if Roompeople.exists?(:user_id => user_id, :room_id => room_id)
        rp = Roompeople.where(:user_id => user_id, :room_id => room_id)
        rp.destroy_all
        flash[:success] = 'success unfollow'
        session[code] = 1
        redirect_to request.referer and return
    end
    flash[:error] = 'you already unfollow'
    redirect_to request.referer and return
  end

  def compile
    if request.post?
        mobile = params[:mobile]
        if mobile == 'android'
            cookies[:username] = params[:username_cookies]
        end
        if cookies[:username] == 'guest'
            redirect_to "/user/login" and return
        end
        u = User.where(:username => cookies[:username]).first
        if u.translate == "0"
            @nontranslate = 1
        end
        code = params[:code]
        @room = Room.where(:code => code).first
        @back = request.referer
        topics = @room.topics
        topic = []
        comment_ids = []
        highlight = Highlight.all
        #like = Like.all
        #hl = highlight + like
        highlight.each do |i|
            comment_ids << i.comment_id
        end
        topics.each do |t|
            comments = []
            commentAll = t.comments.where(:id => comment_ids, :deleted => false).order("created_at ASC")
            if @nontranslate != 1
                commentAll.each do |c|
                    if Commenttranslate.exists?(:comment_id => c.id, :user_id => u.id)
                        ct = Commenttranslate.where(:comment_id => c.id, :user_id => u.id).first
                        comments << [ct, ct.comment.username_as] 
                    else
                        comments << [c, c.username_as]
                    end
                end
            else
                comments = commentAll
            end
            topic << [t, comments]
        end
        @topic = topic
        if mobile == 'android'
           render :json => @topic.to_json and return 
        end
    else
        redirect_to "/" and return
    end
  end


  def readnotif
    code = request.GET[:code]
    expire_action(:controller => 'site', :action => 'roompoll', :code_encrypted => code, :firsttime => 0)
    expire_action(:controller => 'site', :action => 'roompoll', :code_encrypted => code, :firsttime => 1)
    expire_action(:controller => 'site', :action => 'roompoll', :code_encrypted => code, :firsttime => 2)
    user_id = User.where(:username => cookies[:username]).first.id
    topic_id = params[:topic_id]
    n = Notif.where(:topic_id => topic_id)
    n.each do |ni|
        nr = Notifread.where(:user_id => user_id, :notif_id => ni.id)
        nr.destroy_all
    end
    render :nothing => true
  end

  def counterNotifLive
    topic_id = params[:topic_id]
    u = User.where(:username => cookies[:username]).first
    nr = u.notifreads
    nrList = Hash.new(0)
    nr.each do |nri|
        nrList[nri.notif.topic_id] = nrList[nri.notif.topic_id] + 1
    end
    render :json => nrList[topic_id.to_i] and return
  end


  def commenthttp
     comment = params[:datacomment]
     if comment.include? "http"
       comment = comment.split()
       mycom = []
       comment.each do |com|
             if com.include? "http"
                 if com.include? ".png" or com.include? ".jpg"
                     com = "<img src='"+com+"' />"
                     mycom << com
                 else
                     com = "<a href='"+com+"'>"+com+"</a><br>"
                     mycom << com
                 end
             else
                 mycom << CGI::escapeHTML(com)
             end
       end
       comment = mycom.join(" ")
       render :json => comment.to_json and return
     end
  end


  def gcmpostcomment
   registration_id = 'APA91bEaktdlTIEa0XCOyYLMhICglmwu7qTQYXPcds7xMVu5Ma4l9d300_wnV5xtyUfEVTR9L51TJiEeUdo3mpzpieNXeEYWGHUZhxUp6vzdbwmTcdREmxFqnD-Pc9TchjHTR-y14G8Kkl9ivZqbC4NTT9-NUlSYVj28j71bPoxDJE3C5rry6wk'
    message = 'test'
    code = 'asdf'
    username = 'username'
    time = Time.now
    code_un = params[:code_un]
    username = params[:username] 
    #registration_id = params[:registration_id]
    message = URI.unescape(params[:comment])
    topic_id = params[:topic_id]
    #url = "https://android.googleapis.com/gcm/send"
    #uri = URI.parse(url)
    #http = Net::HTTP.new(uri.host, uri.port)
    #if uri.scheme == "https"
    #  http.use_ssl = true
    #  http.verify_mode = OpenSSL::SSL::VERIFY_NONE
    #end
    #fields = { 
    #    :registration_ids => [
    #        registration_id,
    #    ],
    #    :data => {
    #        :message => message,
    #        :code => code_un,
    #        :username => username,
    #        :time => time,
    #        :topic_id => topic_id,
    #    },
    #}
    #payload = fields.to_json
    #myrequest = Net::HTTP::Post.new(uri.request_uri)
    #myrequest["Content-Type"] = "application/json"
    #myrequest["X-HTTP-Method-Override"] = "post"
    #googleapikey = 'AIzaSyABO9gZ7_aoMYNZgzbxIhEvRaJ22fPXRXc'
    #myrequest["Authorization"] = "key="+googleapikey
    #myrequest.body = payload

    # Print the response
    #code = params[:code]
    #if code == 'q'
    code = Room.where(:code => code_un).first.code_encrypted
    #end
    expire_action(:controller => 'site', :action => 'roompoll', :code_encrypted => code, :firsttime => 0)
    expire_action(:controller => 'site', :action => 'roompoll', :code_encrypted => code, :firsttime => 1)
    expire_action(:controller => 'site', :action => 'roompoll', :code_encrypted => code, :firsttime => 2)
    if request.post?
        username = params[:username].html_safe
        username = HTML::FullSanitizer.new.sanitize(username)
        @username = cookies[:username]
        if @username == nil or @username == ''
            @username = params[:username_cookies]
            cookies[:username] = params[:username_cookies]
        end
        comment = CGI::escapeHTML(params[:comment])
        real_comment = params[:comment]
        tab = params[:tab]
        topic_id = params[:topic_id]
        room_id = Room.where(:code => code_un).first.id
        if username == ''
            return
        end
        r = Room.find(room_id)
        if comment == '' or comment == nil
            return
        end
        c = Comment.new(:topic_id => topic_id)
        c.message = comment
        colors = ['94FFA8','FC7EB1','94FAFF']
        user_id = User.where(:username => @username).first.id
        n = Notif.where(:topic_id => topic_id)
        n.each do |ni|
            nr = Notifread.where(:user_id => user_id, :notif_id => ni.id)
            nr.destroy_all
        end
        c.user_id = user_id
        c.username_as = HTML::FullSanitizer.new.sanitize(username)
        c.save
        #REDIS.del(r.code_encrypted)
        #REDIS2.delay.rpush(r.code_encrypted, [c.message, c.user.username, c.topic.title, c.topic.id])
        if Recentcomment.exists?(:room_id => r.id)
            rc = Recentcomment.where(:room_id => r.id).first
            rc.comment_id = c.id
            rc.save
        else
            rc = Recentcomment.new(:room_id => r.id, :comment_id => c.id)
            rc.save
        end
        #Rails.cache.write(r.code_encrypted, [c.message, c.user.username, c.topic.title], :namespace => "roomcomment")
        tmpcomment = comment
        if comment.include? "http"
          comment = comment.split()
          mycom = []
          comment.each do |com|
                if com.include? "http"
                    if com.include? "image:" 
                        com = com.split("image:")[1]
                        com = "<img src='"+com+"' />"
                        mycom << com
                    else
                        com = "<a href='"+com+"'>"+com+"</a><br>"
                        mycom << com
                    end
                else
                    mycom << CGI::escapeHTML(com)
                end
          end
          comment = mycom.join(" ")
        end
        Pusher[code].trigger_async('postcomment', {
          :comment => comment,
          :real_comment => real_comment,
          :username => username,
          :username_real => c.user.username,
          :tab => tab,
          :created_at => Time.now.strftime("%Y-%m-%d %H:%M"), 
          :comment_id => c.id,
          :room_id => room_id,
          :topic_id => topic_id,
          :topic_title => Topic.find(topic_id).title,
          :code => code
        })
        rp = Roompeople.where(:room_id => room_id)
        ios_ids = []
        rp.each do |rps|
            ios_id = rps.user.ios_id
            if ios_id != "0" and ios_id != "(null)"
                ios_ids << ios_id
            end
        end
        notification = {
          :device_tokens => ios_ids,
          :aps => {:alert => "new message: #{comment}", :badge => 1},
          :topic => "#{r.name} - #{Topic.find(topic_id).title}",
          :code => r.code,
        }    
        Urbanairship.push(notification)
        Rails.cache.delete(code+"poll", :namespace => "room")
        comment = tmpcomment
        if !Roompeople.exists?(:user_id => user_id, :room_id => room_id) and cookies[:username] != 'guest'
           rp = Roompeople.new(:user_id => user_id, :room_id => room_id)
           session[code] = 1
           rp.save
        end
        #HardWorker.perform_async(room_id, r, comment, topic_id, cookies[:username], code)
        rp = Roompeople.where(:room_id => room_id)
        rp.each do |p|
            if p.user.username == cookies[:username] || p.user.verified == 0
                next
            end
            n = Notif.new(:message => "got notifications from #{code} room")
            n.topic_id = topic_id
            n.save
            nr = Notifread.new(:user_id => p.user.id, :notif_id => n.id)
            nr.save
            mymail = {
                :recipient => p.user.username,
                :sender => cookies[:username],
                :message => comment,
                :room_name => r.name,
                :room_code => r.code,
                :room_code_encrypted => r.code_encrypted,
                :topic_id => topic_id,
                :host => "#{request.protocol}#{request.host_with_port}"
            }
            begin
                if !p.user.twitter.nil? and p.user.twitter != ''
                   if comment.length > 20
                        commentTwit = comment[0..20]+".."
                   else
                        commentTwit = comment
                   end
                   begin
                       messageTwit = commentTwit+" - "+cookies[:username]+" #{request.protocol}#{request.host_with_port}/site/room/#{r.code_encrypted}?t=#{topic_id}"
                       #Twitter.delay.direct_message_create("#{p.user.twitter}", "#{messageTwit}")
                   end
                else
                    #UserMailer.delay.notification_email(mymail)
                end
            rescue Exception=>e
                test = 'does not do anything'
            end
            #UserMailer.delay.notification_email(mymail)
        end
        
    else
        if Room.where(:code => code).first.topics.exists?
           @topics = Room.where(:code => code).first.topics 
        end
    end
    @code = code
    render :json => "success" and return
    #response = http.request(myrequest)
    #render :text => response.body and return
  end

  def cloudmailin
    headers = params[:headers]  
    subject = headers['Subject']
    sender = headers['From']
    sender_email = sender.split("<")[1].split(">")[0]
    message = params[:plain]  
    topic_id = subject.split("#-")[1].split("-#")[0]
    c = Comment.new(:message => message)
    if User.exists?(:username => sender_email)
        u = User.where(:username => sender_email).first
        c.username_as = u.username_visual
        c.user_id = u.id
    end
    c.topic_id = topic_id.to_i
    c.save
    t = Topic.find(topic_id.to_i)
    topic_id = topic_id.to_i
    room = t.room
    code = room.code_encrypted
    expire_action(:controller => 'site', :action => 'roompoll', :code_encrypted => code, :firsttime => 0)
    expire_action(:controller => 'site', :action => 'roompoll', :code_encrypted => code, :firsttime => 1)
    expire_action(:controller => 'site', :action => 'roompoll', :code_encrypted => code, :firsttime => 2)
    comment = message
    real_comment = message
    username = c.username_as
    room_id = room.id

    Pusher[code].trigger_async('postcomment', {
      :comment => comment,
      :real_comment => real_comment,
      :username => username,
      :username_real => c.user.username,
      :created_at => Time.now.strftime("%Y-%m-%d %H:%M"), 
      :comment_id => c.id,
      :room_id => room_id,
      :topic_id => topic_id,
      :topic_title => Topic.find(topic_id).title,
      :code => code
    })


    render :nothing => true and return
  end


  def translate 
    message = request.GET['m']
  end

end
