require 'uri'

class UserController < ApplicationController

  def index
    if cookies[:username] != 'guest' and cookies[:username] != nil
        redirect_to "/site/home" and return
    end
  end


  def register
    if cookies[:username] != 'guest' and cookies[:username] != nil
        redirect_to "/site/home" and return
    end
    if request.post?
        colors = ['94FFA8','FC7EB1','94FAFF']
        username = params[:username]
        password = params[:password]
        email = params[:username]
        username_visual = params[:username]
        mobile = ''
        mobile = params[:mobile]
        if mobile == 'android'
            username = URI.unescape(username)
            password = URI.unescape(password)
            email = URI.unescape(email)
        end
        if User.exists?(:username => username)
            if mobile == 'android'
                mobileoutput = {
                    :auth => "the username is already exists"
                }
                render :json => mobileoutput and return
            end
            flash[:error] = "the username is already exists"
            redirect_to "/user/login" and return
        end
        if username.include? "@" and username.include? "."
            if !username.include? ".com" and !username.include? ".net" and !username.include? ".org" and !username.include? ".me" and !username.include? ".us"
                flash[:error] = 'you have to put email'
                if mobile == 'android'
                    mobileoutput = {
                        :auth => 'you have to put email'
                    }
                    render :json => mobileoutput and return
                end
                redirect_to "/user/register" and return
            end
        else
            flash[:error] = 'you have to put email'
            if mobile == 'android'
                mobileoutput = {
                    :auth => 'you have to put email'
                }
                render :json => mobileoutput and return
            end
            redirect_to "/user/register" and return
        end
        u = User.new(:username => username, :password => password, :email => email)
        u.username_visual = username_visual
        u.color = colors[rand(2)]
        u.save
        rp = Roompeople.new(:user_id => u.id,  :room_id => Room.where(:code_encrypted => 'd8Q1XO').first.id)
        rp.save
        cookies[:username] = u.username
        flash[:success] = "we have sent verification link to your email, please check to activate"
        mymail = {
            :recipient => email,
            :id => u.id,
            :host => "#{request.protocol}#{request.host_with_port}",
        }
        UserMailer.delay.verification_email(mymail)
        if mobile == 'android'
            mobileoutput = {
                :auth => 'success'
            }
            gcm_id = params[:gcm_id]
            u.gcm_id = gcm_id
            if gcm_id != "" and gcm_id != nil
                u.save
            end
            render :json => mobileoutput and return
        end
        redirect_to "/user/register" and return
    else

    end
  end

  def login
    if request.post?
        email = params[:email]
        password = params[:password]
        mobile = ''
        mobile = params[:mobile]
        if mobile == 'android'
            email = URI.unescape(email)
            password = URI.unescape(password)
        end



        if User.exists?(:email => email, :password => password)
            if mobile == 'android'
                u = User.where(:email => email, :password => password).first
                gcm_id = params[:gcm_id]
                ios_id = params[:iosid]
                if gcm_id != "" and gcm_id != nil
                    u.gcm_id = gcm_id
                    u.save
                end
                if ios_id != "" and ios_id != nil
                    u.ios_id = ios_id
                    u.save
                end
                rp = Roompeople.where(:user_id => u.id)
                nr = Notifread.where(:user_id => u.id)
                rooms = []
                topic_from_nr = []
                nr.each do |nri|
                   if !nri.notif.topic.nil?
                       topic_from_nr << nri.notif.topic 
                   end
                end
                rp.each do |rpi|
                    room = Room.find(rpi.room.id)
                    topic_from_room = room.topics
                    if !room.topics.nil?
                        #if Rails.cache.read(room.code_encrypted, :namespace => "roomcomment")
                        #    last_comment = Rails.cache.read(room.code_encrypted, :namespace => "roomcomment")
                        #    gotCache = 1
                        #else
                        #     if gotCache == 0
                        #         last_comment = REDIS.lrange(room.code_encrypted, 0, 3)
                        #
                        #
                        #
                                 last_comment = Recentcomment.where(:room_id => room.id).first
                                 if !last_comment.nil?
                                     comment_id = last_comment.comment.id
                                 end
                                 common = topic_from_room & topic_from_nr
                                 if common.length > 0
                                     notyetread = 1
                                 end
                                 

                        #         Rails.cache.write(room.code_encrypted, last_comment, :namespace => "roomcomment")
                        #     end
                        #end
                    end
                    if last_comment != nil
                        if notyetread == 1
                            rpi.room.name = "[NEW] " + rpi.room.name
                        end
                    end
                    rooms << [rpi.room.name, rpi.room.code, rpi.room.id]
                end
                render :json => { 
                    :success => 1,
                    :rooms => rooms,
                    :type_translate => u.translate,
                } and return
            end
            u = User.where(:email => email, :password => password).first
            cookies[:username] = u.username
            redirect_to "/site/home" and return
        else
            if mobile == 'android'
                render :json => { 
                :success => 0,
                :username => email
                } and return
            end
            flash[:error] = "the email or password is not match"
            redirect_to request.referer
        end
    else
        if cookies[:username] != 'guest' and cookies[:username] != nil
            redirect_to "/site/home" and return
        end
    end
  end

  def logout
    cookies.delete :username
    reset_session
    redirect_to "/"
  end

  def edit
    if cookies[:username] == 'guest' or cookies[:username] == nil
        redirect_to "/site/home" and return
    end
    if request.post?
        @username = cookies[:username]
        username = params[:username]
        if User.where(:username => username).count > 1
            flash[:error] = 'your username is already exists'
            redirect_to request.referer and return
        end
        password = params[:npassword]
        cpassword = params[:cpassword]
        twitter = params[:twitter]
        if twitter.include? "@"
            flash[:error] = 'twitter username without @'
            redirect_to request.referer and return
        end
        @user = User.where(:username => @username).first
        @user.username_visual = username
        @user.twitter = twitter
        @user.translate = params[:lang]
        if @user.password == cpassword
            if password == '' or password == nil
                flash[:error] = 'you can not put blank password'
                redirect_to request.referer and return
            end
            @user.password = password
        end
        @user.save
        flash[:success] = "success"
        Pusher[@username].trigger_async('changeUserTranslate', {
          :user_translate => @user.translate,
        })
        redirect_to request.referer and return
    else
        @user = User.where(:username => cookies[:username]).first
    end
  end


  def changeColor
        if cookies[:username] == 'guest' or cookies[:username] == nil
            render :nothing => true and return
        end
        color = params[:color]
        color = color[1..6]
        u = User.where(:username => cookies[:username]).first
        u.color = color
        u.save
        render :nothing => true and return
  end

  def activate
    id = params[:id]
    email = request.GET[:email]
    if User.exists?(:id => id)
        u = User.find(id)
        if u.email == email
            u.verified = 1
            u.save
        end
    end
    cookies[:username] = u.username
    flash[:success] = "your account is activated"
    redirect_to "/user/login" and return
  end

  def verification_send
        u = User.where(:username => cookies[:username]).first
        email = u.email
        mymail = {
            :recipient => email,
            :id => u.id,
            :host => "#{request.protocol}#{request.host_with_port}",
        }
        UserMailer.delay.verification_email(mymail)
        flash[:verifysent] = 1
        redirect_to request.referer and return
  end

  def forget
    if request.post?
        email = params[:email]
        if User.exists?(:email => email)
            u = User.where(:email => email).first
            mymail = {
                :recipient => email,
                :id => u.id,
                :host => "#{request.protocol}#{request.host_with_port}",
            }
            UserMailer.delay.forget_email(mymail)
            flash[:success] = "The new random generated password is sent to your email"
            redirect_to "/user/login" and return
        else
            flash[:error] = "The email have not be registered"
            redirect_to request.referer and return
        end
    end
  end

  def resetPassword
        username = request.GET['e']
        pwdcode = request.GET['c']
        new_password = pwdcode[0..4]
        old_password = pwdcode[5..pwdcode.length]
        if User.exists?(:username => username)
            u = User.where(:username => username).first
            pwd = Digest::MD5.hexdigest(u.password)
            if pwd == old_password
                u.password = new_password
                u.save
                flash[:success] = "success reset password, please enter temporary password and you can change that on the setting"
                redirect_to "/user/login" and return
            end
        end
        redirect_to "/user/login" and return
  end

end
