class UserMailer < ActionMailer::Base
  default :from => "e@phischool.com"
 
  def welcome_email(recipient)
    @user = recipient
    @url  = "http://beta.qiscus.com"
    mail(:to => recipient, :subject => "Welcome to Qiscus")
  end

  def invite_email(mymail)
    @recipient = mymail[:recipient]
    @message = mymail[:message]
    @sender = mymail[:sender]
    mail(:to => @recipient, :subject => "#{@sender} invite you to join Qiscus")
  end

  def notification_email(mymail)
    #@recipient = mymail[:recipient]
    #@message = mymail[:message]
    #@sender = mymail[:sender]
    #@code = mymail[:room_code]
    #@host = mymail[:host]
    #@code_encrypted = mymail[:room_code_encrypted]
    #@room_title = mymail[:room_name]
    #@topic_id = mymail[:topic_id]
    #mail(:to => @recipient, :subject => "#{@sender} give a comment on Qiscus")
  end

  def verification_email(mymail)
    @recipient = mymail[:recipient]
    @id = mymail[:id]
    @host = mymail[:host]
    mail(:to => @recipient, :subject => "Qiscus verification email")
  end

  def forget_email(mymail)
    @recipient = mymail[:recipient]
    id = mymail[:id]
    @randomcode = (0...5).map{65.+(rand(26)).chr}.join.downcase
    u = User.find(id)
    @username = u.username
    pwd = Digest::MD5.hexdigest(u.password)
    @code = @randomcode + pwd
    @host = mymail[:host]
    mail(:to => @recipient, :subject => "Qiscus Reset Password")
  end


end
