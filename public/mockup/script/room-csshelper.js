// JavaScript Document

$(document).ready(function(){
	var descOffset = $("#sectionDescription").offset();
	$("#descriptionText").height($("#descriptionText").height() - 23 + (window.innerHeight - (descOffset.top + $("#sectionDescription").height())));
	
	var cmntOffset = $("#sectionComments").offset();
	$("#roomComments").height($("#roomComments").height() - 3 + (window.innerHeight - (cmntOffset.top + $("#sectionComments").height())));
	
	var topicOffset = $("#sectionTopics").offset();
	$("#roomTopics").height($("#roomTopics").height() - 3 + (window.innerHeight - (topicOffset.top + $("#sectionTopics").height())));
});