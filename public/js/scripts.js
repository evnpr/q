jQuery(document).ready(function() {
	setHeightForScrollableArea();
	setGPageContentSize()
	
	$('a.play').click(function() {
		$(this).fadeOut(300, function() {
			$(this).siblings('p').fadeIn(300);
		});

		return false
	});
	
	//show register form
	$('a.register').click(function() {
		$(this).fadeOut(300, function() {
			$('#register-form').fadeIn(300);
		})
		return false
	});
	
	//change background color
	$('.bgcolor nav span').click(function() {
		color = $(this).attr('data-color');
		$('body').css('background', color);
                $.get("/user/changeColor", { color: color });
	});
	
	//toggle messge icon with texting icon
	$('#icon-message').click(function() {
		$(this).toggleClass('icon-texting').toggleClass('icon-message');
	});
	
	//create a new tab when click on ne wtab icon
	var tabTitle = $("#tab_title"), 
		tabContent = $("#tab_content"), 
		tabTemplate = "<li><a href='#{href}'>#{label}</a> <span class='close-tab icon' aria-hidden='true' data-icon='&#xe07a;'></span></li>",
		tabCounter = 2;
	// addTab form: calls addTab function on submit and closes the dialog
	$('#add-tab').click(function() {
		if ($('#tabs').hasClass('ui-tabs')) {
			addTab();
		} else {
			$("#tabs").tabs();
			addTab();
		};
		setHeightForScrollableArea();
	});
	
	// actual addTab function: adds new tab using the input from the form above
	function addTab() {
		var tabs = $('#tabs').tabs();
		var label = "Tab " + tabCounter, 
			id = "tabs-" + tabCounter, 
			li = $(tabTemplate.replace(/#\{href\}/g, "#" + id).replace(/#\{label\}/g, label)), 
			tabContentHtml = $('#conversation').html();
			
		tabs.delegate( "span.close-tab", "click", function() {
			var panelId = $( this ).closest( "li" ).remove().attr( "aria-controls" );
			$( "#" + panelId ).remove();
			tabs.tabs( "refresh" );
		});
		
		tabs.find(".ui-tabs-nav").append(li);
		tabs.children('#room').append("<section class='conversation' id='" + id + "'>" + tabContentHtml + "</section>");
		tabs.tabs("refresh");
		tabCounter++;
	};
	
	$('#modal').dialog({
		modal: true,
		autoOpen: false,
		width:720
	});
	
	$('#modal-new-topic').dialog({
		modal: true,
		autoOpen: false,
		width:720
	});
	
	$('#popup-opener').click(function(){
		$('#modal').dialog('open')
	})
	
	$('#popup-opener-new-topic').click(function(){
		$('#modal-new-topic').dialog('open')
	})
	
	
});

jQuery(window).resize(function() {
	setHeightForScrollableArea();
	setGPageContentSize();
});

//set scrollable area
function setHeightForScrollableArea() {
	bh = $('body').height(); //get the viewport height
	hh = $('#header').height() + 42; //get header height
	fh = $('#type-area').height() + 20; //get footer height
	th = $('.tabs-nav-container').height(); //get tabs nav height
	height = bh - hh - fh - th - 105;
	$('.scrollable').css('height', height);
};

//set scrollable area for g-page
function setGPageContentSize() {
	bh = $('body').height(); //get the viewport height
	hh = $('#header').height() + 42; //get header height
	height = bh - hh;
	
	bw = $('body').width(); //get the viewport width
	cw = $('#g-page-container').width() // get container width
	outer = (bw - cw) / 2;
	
	$('#left-sidebar').css('height', height)
	
	$('#g-page-content').css({
		'height': height,
		'paddingRight': outer,
		'marginRight': -outer
	});
};

document.createElement("article");
document.createElement("footer");
document.createElement("header");
document.createElement("hgroup");
document.createElement("nav");
document.createElement("aside");
document.createElement("section"); 
