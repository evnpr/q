class CreateNotifs < ActiveRecord::Migration
  def change
    create_table :notifs do |t|
      t.string :message
      t.integer :type

      t.timestamps
    end
  end
end
