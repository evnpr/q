class CreateRecentcomments < ActiveRecord::Migration
  def change
    create_table :recentcomments do |t|
      t.integer :room_id
      t.integer :comment_id

      t.timestamps
    end
  end
end
