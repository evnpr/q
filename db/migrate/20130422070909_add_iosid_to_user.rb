class AddIosidToUser < ActiveRecord::Migration
  def change
    add_column :users, :ios_id, :string, :default => 0
  end
end
