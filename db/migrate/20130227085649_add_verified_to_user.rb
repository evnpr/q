class AddVerifiedToUser < ActiveRecord::Migration
  def change
    add_column :users, :verified, :integer, :default => 0
  end
end
