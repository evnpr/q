class RemoveColorFromHighlight < ActiveRecord::Migration
  def up
    remove_column :highlights, :color
  end

  def down
    add_column :highlights, :color, :integer
  end
end
