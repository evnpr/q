class AddGcmidToUser < ActiveRecord::Migration
  def change
    add_column :users, :gcm_id, :integer, :default => 0
  end
end
