class RemoveCodeencryptedFromRoom < ActiveRecord::Migration
  def up
    remove_column :rooms, :code_encrypted
  end

  def down
    add_column :rooms, :code_encrypted, :string
  end
end
