class CreateQuoras < ActiveRecord::Migration
  def change
    create_table :quoras do |t|
      t.text :html

      t.timestamps
    end
  end
end
