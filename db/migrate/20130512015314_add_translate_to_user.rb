class AddTranslateToUser < ActiveRecord::Migration
  def change
    add_column :users, :translate, :string, :default => "0"
  end
end
