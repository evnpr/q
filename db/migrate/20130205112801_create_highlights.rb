class CreateHighlights < ActiveRecord::Migration
  def change
    create_table :highlights do |t|
      t.integer :user_id
      t.integer :comment_id
      t.integer :color

      t.timestamps
    end
  end
end
