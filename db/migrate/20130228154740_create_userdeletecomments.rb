class CreateUserdeletecomments < ActiveRecord::Migration
  def change
    create_table :userdeletecomments do |t|
      t.integer :user_id
      t.integer :comment_id

      t.timestamps
    end
  end
end
