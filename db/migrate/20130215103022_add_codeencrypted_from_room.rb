class AddCodeencryptedFromRoom < ActiveRecord::Migration
  def change
    add_column :rooms, :code_encrypted, :string
  end
end
