class CreateCommenttranslates < ActiveRecord::Migration
  def change
    create_table :commenttranslates do |t|
      t.integer :user_id
      t.integer :comment_id
      t.text :message

      t.timestamps
    end
  end
end
