class ChangeGcmToString < ActiveRecord::Migration
  def up
    change_column :users, :gcm_id, :string
  end

  def down
    change_column :users, :gcm_id, :integer
  end
end
