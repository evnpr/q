class AddCodeencryptedToRoom < ActiveRecord::Migration
  def change
    add_column :rooms, :code_encrypted, :text
  end
end
