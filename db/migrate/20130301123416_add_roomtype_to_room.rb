class AddRoomtypeToRoom < ActiveRecord::Migration
  def change
    add_column :rooms, :room_type, :integer, :default => 0
  end
end
